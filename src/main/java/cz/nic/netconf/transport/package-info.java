/**
 * Contains everything related to the 
 * transport protocol of Netconf messages.
 */
package cz.nic.netconf.transport;