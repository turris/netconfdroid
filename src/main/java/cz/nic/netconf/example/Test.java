package cz.nic.netconf.example;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.w3c.dom.Document;

import cz.nic.netconf.CapabilityException;
import cz.nic.netconf.CloseSession;
import cz.nic.netconf.Commit;
import cz.nic.netconf.CopyConfig;
import cz.nic.netconf.CreateSubscription;
import cz.nic.netconf.Datastore;
import cz.nic.netconf.DeleteConfig;
import cz.nic.netconf.DiscardChanges;
import cz.nic.netconf.Get;
import cz.nic.netconf.Get.GetReply;
import cz.nic.netconf.GetConfig;
import cz.nic.netconf.GetConfig.GetConfigReply;
import cz.nic.netconf.KillSession;
import cz.nic.netconf.Lock;
import cz.nic.netconf.PartialLock;
import cz.nic.netconf.PartialUnlock;
import cz.nic.netconf.Unlock;
import cz.nic.netconf.Validate;
import cz.nic.netconf.XPathSelections;
import cz.nic.netconf.frame.RpcHandler;
import cz.nic.netconf.frame.RpcReply;
import cz.nic.netconf.messages.DOMUtils;
import cz.nic.netconf.transport.NetconfCatcher;
import cz.nic.netconf.transport.NetconfCatcherListener;
import cz.nic.netconf.transport.NetconfTransportEvent;
import cz.nic.netconf.transport.Session;
import cz.nic.netconf.transport.ssh.NetconfSshCatcher;
import cz.nic.netconf.transport.ssh.SshAuthInfo;

public class Test
{

	public static final void main(String s[]) throws IOException 
	{
		/* Prepare the connection */
		InetSocketAddress addr = new InetSocketAddress("vhosting.eu.org", 22);
        NetconfSshCatcher catcher = new NetconfSshCatcher("test", null, new SshAuthInfo(addr, "netconf", "net1231"));
		
        /* The log is full enabled. */
        NetconfCatcher.enableLog(NetconfCatcher.LogLevel.MESSAGES);
        
        /* The body of the application */
        catcher.setNetconfCatcherListener(new NetconfCatcherListener()
		{
			
			@Override
			public void processTransportEvents(NetconfTransportEvent event) {
				System.out.println("An event occur.");
			}
			
			@Override
			public void processReadyForRpcRequests(RpcHandler rpcHandler) {
				
				try
				{
					copyConfig(rpcHandler);
					
					deleteConfig(rpcHandler);
					
					commit(rpcHandler);
					
					createSubscription(rpcHandler);
					
					discardChanges(rpcHandler);
					
					get(rpcHandler);
					
					getConfig(rpcHandler);
					
					killSession(rpcHandler);
					
					lock(rpcHandler);
					
					partialLock(rpcHandler);
					
					partialUnLock(rpcHandler);
					
					unlock(rpcHandler);
					
					validate(rpcHandler);
					
				    testOnCloseSession(rpcHandler);
				}
				catch(Exception e)
				{
					e.printStackTrace();
					System.out.println("Impossibile inviare il messaggio: Connessione chiusa.");
				}
			}
			
		
		});
        
        
        Runnable connection = catcher.getRunnableConnection(10000);
		/* Start the connection */
		new Thread(connection).start();
	}
	
	private static void testOnCloseSession(RpcHandler rpcHandler) throws IOException {
		CloseSession cs = new CloseSession(rpcHandler.getSession());
		cs.executeSync(rpcHandler);
	}
	private static void copyConfig(RpcHandler rpcHandler) throws IOException, CapabilityException {
		CopyConfig cc = new CopyConfig(rpcHandler.getSession(), Datastore.candidate, Datastore.running);
		cc.executeSync(rpcHandler);
	}
	private static void deleteConfig(RpcHandler rpcHandler) throws IOException, CapabilityException {
		DeleteConfig cc = new DeleteConfig(rpcHandler.getSession(), Datastore.candidate);
		cc.executeSync(rpcHandler);
	}
	private static void commit(RpcHandler rpcHandler) throws IOException, CapabilityException {
		Commit cc = new Commit(rpcHandler.getSession());
		cc.executeSync(rpcHandler);
	}
	private static void createSubscription(RpcHandler rpcHandler) throws IOException, CapabilityException {
		CreateSubscription cc = new CreateSubscription(rpcHandler.getSession());
		cc.executeSync(rpcHandler);
	}
	private static void discardChanges(RpcHandler rpcHandler) throws IOException, CapabilityException {
		DiscardChanges cc = new DiscardChanges(rpcHandler.getSession());
		cc.executeSync(rpcHandler);
		
	}
	private static void get(RpcHandler rpcHandler) throws IOException, CapabilityException {
		Get cc = new Get(rpcHandler.getSession());
		RpcReply reply = cc.executeSync(rpcHandler);
		GetReply get = cc.new GetReply(reply);
		printXmlData(get.getData());
	}
	private static void getConfig(RpcHandler rpcHandler) throws IOException, CapabilityException {
		GetConfig cc = new GetConfig(rpcHandler.getSession(), Datastore.running);
		RpcReply reply = cc.executeSync(rpcHandler);
		GetConfigReply gcr = cc.new GetConfigReply(reply);
		printXmlData(gcr.getData());

	}
	private static void printXmlData(Document data) throws IOException {
		System.out.println("\n## DATA AS XML DOCUMENT ##");
		DOMUtils.dump(data, System.out);
		System.out.println("## END OF DATA AS XML DOCUMENT ##");
	}

	private static void killSession(RpcHandler rpcHandler) throws IOException, CapabilityException {
		KillSession cc = new KillSession(rpcHandler.getSession(), 3);
		cc.executeSync(rpcHandler);
	}
	private static void lock(RpcHandler rpcHandler) throws IOException, CapabilityException {
		
		Lock cc = new Lock(rpcHandler.getSession(), Datastore.candidate);
		cc.executeSync(rpcHandler);
	}
	private static void partialLock(RpcHandler rpcHandler) throws IOException, CapabilityException {
		XPathSelections sel = new XPathSelections();
		sel.addSelection("/", Session.BASE_1_0);
		PartialLock cc = new PartialLock(rpcHandler.getSession(), sel);
		cc.executeSync(rpcHandler);
	}
	private static void partialUnLock(RpcHandler rpcHandler) throws IOException, CapabilityException {

		PartialUnlock cc = new PartialUnlock(rpcHandler.getSession(), 3);
		cc.executeSync(rpcHandler);
	}
	private static void unlock(RpcHandler rpcHandler) throws IOException, CapabilityException {
		Unlock cc = new Unlock(rpcHandler.getSession(), Datastore.candidate);
		cc.executeSync(rpcHandler);
	}
	private static void validate(RpcHandler rpcHandler) throws IOException, CapabilityException {
		Validate cc = new Validate(rpcHandler.getSession(), Datastore.candidate);
		cc.executeSync(rpcHandler);
	}
	
}
