/**
 * Contains classes used by the system 
 * and that serve to define the XML messages of Netconf protocol.
 * Normally you'll never need to access these classes.
 */
package cz.nic.netconf.messages;

